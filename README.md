# Quin QA Challenge

This project is the solution for the QA challenge question set up by Miro. Please find below some key features,
and the steps on how to execute the project.

**Key Features:**

- Framework used : Cucumber, Rest Assured, TestNG
- Coding Language : Java (Executed with 8, compatible with 13)
- Build Tool : Maven 3.6.3
- Logging : Log4j2
- Reporting : Maven Cucumber Reporting Plugin
- Suggested IDE Plugins : Cucumber for Java & Gherkin (Intellij Idea Plugins)
- CI Tool Used: GitLab

**General Information:**
- Module is divided into two modules:
    - Core : Contains all the dependencies and utilities used for the solution
    - Test : Contains the actual test cases and execution profiles of the solution


_P.S : Reason to do it in such a way is to make sure that a new project can right away make use of this framework
by just adding the dependency of the core-module._

- The file cucumber.properties (@src/test/resources) contains the base URL. In case the URL
  is changed, it should only be changed in this file.

- Testcases (scenarios) are written in Gherkin @src/test/java/features
- Runner is present @src/test/runners/CukeRunnerTestNG.java.

**Execution Instructions:**
- `mvn clean install` : Cleans the target folder and sets the Base URL for the execution
- `mvn verify -Prun-test-cases` : Executes the project and generates the report
- Alternatively, the runner file or the testng.xml file can be executed in order to test. Please note that the
  report cannot be generated this way.
- Reports can be found at : quin-qa-test/target/cucumber-reports
- Logs can be found with the name quin-qa-test/quin-qa-challenge-(timestamp).log
- Reports are also hosted @https://reports.cucumber.io/ (is only valid for 24 hours). Complete link is always logged into the console for reference.


Please connect with the author (pintudinesh@gmail.com || pintudinesh-1912(github name)) in case there is any issue in accessing the project and/or executing the same.

