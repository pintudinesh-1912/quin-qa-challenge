Feature: Test whether Create/Read/Update/Delete operations work successfully for Bins.io

  @create
  Scenario Outline: Verify whether a bin is successfully created with different set of data
    When The bin visibility is set to "<Bin Visibility>"
    And  The bin name is set to "<Bin Name>"
    And  The collection id is provided as "<Collection ID>"
    And  The body is set from "<Body>"
    When The Post api is triggered for the bin APIs
    Then Verify that the API call results as per "<Expected Result>"
    Examples:
      | Bin Visibility | Bin Name   | Collection ID | Body    | Expected Result |
      | Private        | Valid Name | None          | Default | Successful      |
      | Public         | Valid Name | None          | Default | Successful      |
      | None           | Valid Name | None          | Default | Successful      |
      | None           | Valid Name | None          | None    | Unsuccessful    |
      | Private        | Valid Name | None          | None    | Unsuccessful    |

  @read
  Scenario: Verify that a bin can be retrieved successfully with the help of bin id
    When A random bin is created
    Then Verify that the bin is created successfully
    When The bin is retrieved using the bin id
    Then Verify that the bin is retrieved successfully

  @read
  Scenario: Verify that a bin can be retrieved successfully without the metadata
    When A random bin is created
    Then Verify that the bin is created successfully
    When The bin is retrieved using the bin id without the metadata
    Then Verify that the bin is retrieved successfully without the metadata

  @update
  Scenario Outline: Verify that a specific bin can be updated with the bin id
    When A random bin is created
    Then Verify that the bin is created successfully
    When The update body data is picked up from "<Update Data>"
    When The bin versioning is set as per "<Bin Versioning>"
    When The update API is triggered
    Then Verify that the API response code is as per "<Expected Response Code>"
    Then Verify that the API response message is as per "<Expected Response Message>"
    Examples:
      | Update Data      | Bin Versioning | Expected Response Code | Expected Response Message |
      | Update Dataset 1 | False          | 200                    | Record Updated            |
      | Update Dataset 1 | True           | 403                    | Pro Users Only            |
      | None             | False          | 400                    | Invalid Body              |

  @delete
  Scenario: Verify that a bin can be deleted using a bin id
    When A random bin is created
    Then Verify that the bin is created successfully
    When The delete api is triggered for the bin
    Then Verify that the bin is deleted successfully
    When The get api is triggered for the deleted bin
    Then Verify that the bin is no longer found

  @delete
  Scenario: Verify that a bin is not deleted when an invalid bin id is passed on to the delete api
    When A random bin is created
    Then Verify that the bin is created successfully
    When The delete api is triggered for the bin with invalid id
    Then Verify that none of the bins are deleted
    When The bin is retrieved using the bin id
    Then Verify that the bin is retrieved successfully