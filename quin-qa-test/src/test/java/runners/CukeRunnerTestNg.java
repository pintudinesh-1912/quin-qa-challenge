package runners;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = {"src/test/java/features"},
        glue = {"steps"},
        monochrome = true,
        plugin = {"json:target/JSONReport/JSONReport.json"}
)
public class CukeRunnerTestNg extends AbstractTestNGCucumberTests {
}
