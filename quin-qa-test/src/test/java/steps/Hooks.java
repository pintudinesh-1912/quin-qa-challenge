package steps;

import io.cucumber.java.Before;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

import static utils.ReadPropertiesFile.loadProperty;

public class Hooks {

    private static Response response;

    public static Response getResponse() {
        return response;
    }

    public static void setResponse(Response response) {
        Hooks.response = response;
    }

    @Before
    public void initialSetup() {
        RestAssured.baseURI = loadProperty().getProperty("base.url");
        RestAssured.basePath = loadProperty().getProperty("base.path");

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build();
    }
}
