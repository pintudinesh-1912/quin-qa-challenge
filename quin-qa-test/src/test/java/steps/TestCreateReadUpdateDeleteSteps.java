package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static endpoints.CrudEndpoints.createBinEndpoint;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.*;
import static steps.Hooks.getResponse;
import static steps.Hooks.setResponse;
import static utils.HandleJsonFiles.readFromJsonFile;
import static utils.ReadPropertiesFile.loadProperty;

public class TestCreateReadUpdateDeleteSteps {

    private String binVisibility;
    private String binName;
    private String collectionId;
    private String body;
    private String generatedId;
    private String binVersioning;

    private final Logger logger = LogManager.getLogger(TestCreateReadUpdateDeleteSteps.class);

    @When("The bin visibility is set to {string}")
    public void theBinVisibilityIsSetTo(String binVisibility) {
        switch (binVisibility.toLowerCase()) {
            case "true":
            case "private":
                this.binVisibility = "true";
                logger.info("Bin visibility is set to:" + "Private");
                break;
            case "false":
            case "public":
                this.binVisibility = "false";
                logger.info("Bin visibility is set to:" + "Public");
                break;
            case "none":
                this.binVisibility = "";
                logger.info("Bin visibility is set to:" + "");
                break;
            default:
                logger.fatal("Invalid value is passed for X-Bin-Private header " + binVisibility);
                throw new RuntimeException("Error in bin setting bin visibility");
        }
    }

    @And("The bin name is set to {string}")
    public void theBinNameIsSetTo(String binName) {
        switch (binName) {
            case "Valid Name":
                this.binName = RandomStringUtils.randomAlphabetic(8);
                break;
            case "None":
                this.binName = "";
                break;
            default:
                this.binName = binName;
        }
        logger.info("Bin name is set to:" + this.binName);
    }

    @And("The collection id is provided as {string}")
    public void theCollectionIdIsProvidedAs(String collectionId) {
        switch (collectionId) {
            case "Valid Name":
                this.collectionId = RandomStringUtils.randomAlphanumeric(10);
                break;
            case "None":
                this.collectionId = "";
                break;
            default:
                this.collectionId = collectionId;
        }
        logger.info("Collection id is set to:" + collectionId);
    }

    @And("The body is set from {string}")
    public void theBodyIsSetFrom(String bodyPath) {
        if (bodyPath.equalsIgnoreCase("None")) {
            body = "";
        } else {
            body = readFromJsonFile(loadProperty().getProperty("default.test.data") + "/bodyData.json", bodyPath).toString();
        }
        logger.info("Body text set to:" + body);
    }

    @When("The Post api is triggered for the bin APIs")
    public void thePostApiIsTriggeredForTheBinAPIs() {
        setResponse(given().header("X-Bin-Private", binVisibility)
                .header("X-Bin-Name", binName)
                .header("X-Collection-Id", collectionId)
                .header("X-Master-Key", loadProperty().getProperty("master.key"))
                .body(body).log().all().post(createBinEndpoint));
    }

    @Then("Verify that the API call results as per {string}")
    public void verifyThatTheAPICallResultsAsPer(String expectedResult) {

        switch (expectedResult) {
            case "Successful":
                assertEquals(getResponse().statusCode(), 200);
                assertEquals(getResponse().path("metadata.name").toString(), binName);
                if (binVisibility.equalsIgnoreCase("")) {
                    assertEquals(getResponse().path("metadata.private").toString(), "true");
                } else {
                    assertEquals(getResponse().path("metadata.private").toString(), binVisibility);
                }
                if (!binName.equals("")){
                    assertEquals(getResponse().path("metadata.name").toString(), binName);
                }
                break;
            case "Unsuccessful":
                assertEquals(getResponse().getStatusCode(), 400);
                break;
        }
    }

    @When("A random bin is created")
    public void aRandomBinIsCreated() {
        body = readFromJsonFile(loadProperty().getProperty("default.test.data") + "/bodyData.json", "Default").toString();
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .body(body).post(createBinEndpoint));
    }

    @Then("Verify that the bin is created successfully")
    public void verifyThatTheBinIsCreatedSuccessfully() {
        assertEquals(getResponse().statusCode(), 200);
        generatedId = getResponse().path("metadata.id");
    }

    @When("The bin is retrieved using the bin id")
    public void theBinIsRetrievedUsingTheBinId() {
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .pathParam("binId",generatedId)
                .get(createBinEndpoint + "/{binId}"));
    }

    @Then("Verify that the bin is retrieved successfully")
    public void verifyThatTheBinIsRetrievedSuccessfully() {
        assertEquals(getResponse().getStatusCode(),200);
        assertEquals(getResponse().path("metadata.id"),generatedId);
    }

    @When("The bin is retrieved using the bin id without the metadata")
    public void theBinIsRetrievedUsingTheBinIdWithoutTheMetadata() {
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .header("X-Bin-Meta","false")
                .pathParam("binId",generatedId)
                .get(createBinEndpoint + "/{binId}"));
    }

    @Then("Verify that the bin is retrieved successfully without the metadata")
    public void verifyThatTheBinIsRetrievedSuccessfullyWithoutTheMetadata() {
        assertEquals(getResponse().getStatusCode(),200);
        assertNull(getResponse().path("metadata"));
    }

    @When("The update body data is picked up from {string}")
    public void theUpdateBodyDataIsPickedUpFrom(String updateBody) {
        body = readFromJsonFile(loadProperty().getProperty("default.test.data") + "/bodyData.json", updateBody).toString();
    }

    @When("The bin versioning is set as per {string}")
    public void theBinVersioningIsSetAsPer(String binVersioning) {
        switch (binVersioning.toLowerCase()) {
            case "True":
            case "true":
                this.binVersioning = "true";
                break;
            case "False":
            case "false":
                this.binVersioning = "false";
                break;
            default:
                logger.debug("Invalid bin version set for the update request");
        }
    }

    @When("The update API is triggered")
    public void theUpdateAPIIsTriggered() {
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .header("X-Bin-Meta","false")
                .header("X-Bin-Versioning", binVersioning)
                .pathParam("binId",generatedId)
                .body(body)
                .put(createBinEndpoint + "/{binId}"));
    }

    @Then("Verify that the API response code is as per {string}")
    public void verifyThatTheAPIResponseCodeIsAsPer(String expectedResponseCode) {
        assertEquals(getResponse().statusCode(),Integer.parseInt(expectedResponseCode));
    }

    @Then("Verify that the API response message is as per {string}")
    public void verifyThatTheAPIResponseMessageIsAsPer(String expectedResponseMessage) {
        switch (expectedResponseMessage) {
            case "Record Updated":
                assertEquals(getResponse().path("record").toString(),body.replace("\"","").replace(":","="));
                break;
            case "Pro Users Only":
                assertTrue(getResponse().path("message").toString().contains("Versioning is not available for the Free users"));
                break;
            case "Invalid Body":
                assertTrue(getResponse().path("message").toString().contains("Bin cannot be blank"));
                break;
            default:
                throw new RuntimeException("Invalid Expected Response Message for Update API");
        }
    }

    @When("The delete api is triggered for the bin")
    public void theDeleteApiIsTriggeredForTheBin() {
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .pathParam("binId",generatedId)
                .delete(createBinEndpoint + "/{binId}"));
    }

    @Then("Verify that the bin is deleted successfully")
    public void verifyThatTheBinIsDeletedSuccessfully() {
        assertEquals(getResponse().getStatusCode(),200);
        assertEquals(getResponse().path("metadata.id").toString(),generatedId);
        assertEquals(getResponse().path("message").toString(),"Bin deleted successfully");
    }

    @When("The get api is triggered for the deleted bin")
    public void theGetApiIsTriggeredForTheDeletedBin() {
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .pathParam("binId",generatedId)
                .get(createBinEndpoint + "/{binId}"));
    }

    @Then("Verify that the bin is no longer found")
    public void verifyThatTheBinIsNoLongerFound() {
        assertEquals(getResponse().getStatusCode(),404);
        assertEquals(getResponse().path("message").toString(),"Bin not found or it doesn't belong to your account");
    }

    @When("The delete api is triggered for the bin with invalid id")
    public void theDeleteApiIsTriggeredForTheBinWithInvalidId() {
        setResponse(given().header("X-Master-Key", loadProperty().getProperty("master.key"))
                .pathParam("binId","618a552f763da443125de7a0")
                .delete(createBinEndpoint + "/{binId}"));
    }

    @Then("Verify that none of the bins are deleted")
    public void verifyThatNoneOfTheBinsAreDeleted() {
        assertEquals(getResponse().getStatusCode(),404);
        assertEquals(getResponse().path("message").toString(),"Bin not found or it doesn't belong to your account");
    }
}
