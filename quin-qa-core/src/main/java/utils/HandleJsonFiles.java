package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class HandleJsonFiles {

    private static final Logger logger = LogManager.getLogger(HandleJsonFiles.class);

    public static JSONObject readFromJsonFile(String filePath, String searchKey) {
        filePath = System.getProperty("user.dir") + filePath;
        JSONParser jsonParser = new JSONParser();
        JSONObject searchResult = new JSONObject();

        try {
            Object object = jsonParser.parse(new FileReader(filePath));
            JSONObject jsonObject = (JSONObject) object;
            searchResult = (JSONObject) jsonObject.get(searchKey);
        } catch (Exception e) {
            logger.fatal("Unable to read from json file at " + filePath);
        }
        return searchResult;
    }
}
