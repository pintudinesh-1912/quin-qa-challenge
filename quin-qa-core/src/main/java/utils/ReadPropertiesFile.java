package utils;

import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadPropertiesFile {

    public static Properties loadProperty() {
        ClassPathResource classPathResource = new ClassPathResource("cucumber.properties");
        Properties properties = new Properties();
        try (InputStream inputStream = classPathResource.getInputStream()) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException("Unable to read cucumber properties");
        }
        return properties;
    }
}
